package com.cognac.fundamental

class Lazy[A](f: => A) {
  private var _isset = false
  private var _value: A = _

  def value: A = {
    if (!_isset) { _isset = true; _value = f }
    _value
  }

}

object Lazy {
  def apply[A](f: => A): Lazy[A] = new Lazy[A](f)
}
