package com.cognac.fundamental.collection

import com.cognac.fundamental.collection.DLNodeListLike._

trait DLNodeListLike[L <: DLNodeListLike[L, N], N <: Node[L, N]] { self: L =>
  private var _size: Int = 0

  private var _head: N = _
  private var _last: N = _

  @inline final def hasHead: Boolean = _head != null
  @inline final def headNodeOrNull: N = _head
  @inline final def headNodeOption: Option[N] = Option(_head)
  @inline final def headNode: N = if (_head != null) _head else throw new NoSuchElementException

  @inline final def hasLast: Boolean = _last != null
  @inline final def lastNodeOrNull: N = _last
  @inline final def lastNodeOption: Option[N] = Option(_last)
  @inline final def lastNode: N = if (_last != null) _last else throw new NoSuchElementException

  def insertHead(node: N): N = {
    if (node.isAttached) node.remove()
    node._prev = null.asInstanceOf[N]
    node._next = _head
    if (_head != null) _head._prev = node else _last = node
    _head = node
    node._list = this
    _size += 1
    node
  }

  def insertLast(node: N): N = {
    if (node.isAttached) node.remove()
    node._next = null.asInstanceOf[N]
    node._prev = _last
    if (_last != null) _last._next = node else _head = node
    _last = node
    node._list = this
    _size += 1
    node
  }

  def removeHead(): N = if (_head != null) {
    _head.remove()
  } else throw new NoSuchElementException

  def removeLast(): N = if (_last != null) {
    _last.remove()
  } else throw new NoSuchElementException

  def clear(): Unit = {
    nodeIterator.foreach(_._list = null.asInstanceOf[L])
    _head = null.asInstanceOf[N]
    _last = null.asInstanceOf[N]
    _size = 0
  }

  def nodeIterator: Iterator[N] = new Iterator[N] {
    private var node = headNodeOrNull

    override def hasNext: Boolean =
      node != null

    override def next(): N = {
      val _node = node
      node = node.nextOrNull
      _node
    }
  }

}

object DLNodeListLike {

  trait Node[L <: DLNodeListLike[L, N], N <: Node[L, N]] { self: N =>
    protected[DLNodeListLike] var _list: L = _

    def list: L = _list

    protected[DLNodeListLike] var _prev: N = _
    protected[DLNodeListLike] var _next: N = _

    def hasNext: Boolean = _next != null
    def nextOrNull: N = _next
    def nextOption: Option[N] = Option(_next)
    def next: N = if (_next != null) _next else throw new NoSuchElementException

    def hasPrev: Boolean = _prev != null
    def prevOrNull: N = _prev
    def prevOption: Option[N] = Option(_prev)
    def prev: N = if (_prev != null) _prev else throw new NoSuchElementException

    def insertNext(that: N): N = {
      assertIsAttached()
      if (that.isAttached) that.remove()
      that._next = _next
      that._prev = this
      if (_next != null) _next._prev = that else list._last = that
      _next = that
      that._list = _list
      _list._size += 1
      that
    }

    def insertPrev(that: N): N = {
      assertIsAttached()
      if (that.isAttached) that.remove()
      that._prev = _prev
      that._next = this
      if (_prev != null) _prev._next = that else list._head = that
      _prev = that
      that._list = this._list
      _list._size += 1
      that
    }

    def remove(): N = {
      assertIsAttached()
      if (_prev != null) _prev._next = _next else list._head = _next
      if (_next != null) _next._prev = _prev else list._last = _prev
      _list._size -= 1
      _prev = null.asInstanceOf[N]
      _next = null.asInstanceOf[N]
      _list = null.asInstanceOf[L]
      this
    }

    override def toString: String = {
      val p = if (_prev != null) "<" else "-"
      val n = if (_next != null) ">" else "-"
      s"Node($p$n)"
    }

    @inline protected[DLNodeListLike] def assertIsAttached(): Unit = if (!isAttached) throw new IllegalStateException

    @inline protected[DLNodeListLike] def isAttached: Boolean = _list != null

  }

}

