package com.cognac.fundamental.collection

class DLNodeList[A] extends DLNodeListLike[DLNodeList[A], DLNodeList.Node[A]] with Iterable[A] {
  override def iterator: Iterator[A] = nodeIterator.map(_.value)
}

object DLNodeList {

  class Node[A](val value: A) extends DLNodeListLike.Node[DLNodeList[A], Node[A]]

  def empty[A]: DLNodeList[A] = new DLNodeList[A]

}
