package com.cognac.fundamental.collection

trait Cursor[A] {
  def isAtEnd: Boolean
  def current: A
  def advance(): Boolean
}
