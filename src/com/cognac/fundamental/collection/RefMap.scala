package com.cognac.fundamental.collection

import java.util.{ IdentityHashMap => JIMap }
import scala.collection.{ IterableOnce, MapFactory, mutable }

class RefMap[A, B] private (data: JIMap[A, B])
  extends mutable.Map[A, B] with mutable.MapOps[A, B, RefMap, RefMap[A, B]] {

  def this(items: IterableOnce[(A, B)]) = this {
    val map = new JIMap[A, B]
    items.iterator.foreach(e => map.put(e._1, e._2))
    map
  }

  override def mapFactory: MapFactory[RefMap] = {
    new MapFactory[RefMap] {
      override def empty[K, V]: RefMap[K, V] = new RefMap[K, V](Iterable.empty)

      override def from[K, V](it: IterableOnce[(K, V)]): RefMap[K, V] = new RefMap[K, V](it)

      override def newBuilder[K, V]: mutable.Builder[(K, V), RefMap[K, V]] = new RefMap.Builder[K, V]
    }
  }

  protected override def fromSpecific(coll: IterableOnce[(A, B)]): RefMap[A, B] =
    new RefMap(coll)

  protected override def newSpecificBuilder: mutable.Builder[(A, B), RefMap[A, B]] =
    new RefMap.Builder[A, B]

  override def empty: RefMap[A, B] = new RefMap[A, B](Iterable.empty)

  override def addOne(elem: (A, B)): RefMap.this.type = {
    val (key, value) = elem
    data.put(key, value)
    this
  }

  override def subtractOne(elem: A): RefMap.this.type = {
    data.remove(elem)
    this
  }

  override def get(key: A): Option[B] = {
    if (data.containsKey(key)) Option(data.get(key)) else None
  }

  override def iterator: Iterator[(A, B)] = new Iterator[(A, B)] {
    private val wrapped = data.entrySet().iterator()
    override def hasNext: Boolean = wrapped.hasNext
    override def next(): (A, B) = {
      val e = wrapped.next()
      (e.getKey, e.getValue)
    }
  }

}

object RefMap {

  def empty[A <: AnyRef, B] = new RefMap[A, B](Iterable.empty)

  def apply[A <: AnyRef, B](items: (A, B)*): RefMap[A, B] = new RefMap[A, B](items)

  class Builder[A, B] extends mutable.Builder[(A, B), RefMap[A, B]] {
    private val buffer = mutable.ArrayBuffer.empty[(A, B)]

    override def clear(): Unit = buffer.clear()

    override def result(): RefMap[A, B] = new RefMap[A, B](buffer)

    override def addOne(elem: (A, B)): Builder.this.type = {
      buffer.append(elem)
      this
    }
  }

}
