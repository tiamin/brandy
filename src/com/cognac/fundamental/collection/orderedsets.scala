package com.cognac.fundamental.collection

trait ContinuousOrderedSet[A <: Ordered[A]] {
  def apply(point: A): Boolean
  def range(point: A): Option[ContinuousRange[A]]
  def nextRange(point: A): Option[ContinuousRange[A]]
  def prevRange(point: A): Option[ContinuousRange[A]]
  def nextStart(point: A): Option[A]
}

trait ContinuousRangeLimit[A <: Ordered[A]]

final case class ClosedLimit[A <: Ordered[A]](value: A) extends ContinuousRangeLimit[A]

final case class OpenLimit[A <: Ordered[A]](value: A) extends ContinuousRangeLimit[A]

final case class ContinuousRange[A <: Ordered[A]](lo: ContinuousRangeLimit[A], hi: ContinuousRangeLimit[A])
