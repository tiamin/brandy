package com.cognac.fundamental.collection

import com.cognac.fundamental.TypeUtils.{ baseTypes, resolveType }

import scala.reflect.runtime.universe._

class TypeSet[A] {
  private var map = Map.empty[Type, A]
  private var list = List.empty[A]

  def +=[X <: A : TypeTag](value: X): Unit = {
    val tpe = resolveType[X]
    if (!map.contains(tpe)) {
      map += tpe -> value
      list = value :: list
    }
  }

  def -=[X <: A : TypeTag](): Unit = {
    val tpe = resolveType[X]
    map.get(tpe) foreach { v =>
      map -= tpe
      list = list.filterNot(_ == v)
    }
  }

  def apply[X <: A : TypeTag]: Option[X] = {
    baseTypes[X].foreach { baseType =>
      map.get(baseType).foreach(v => return Some(v.asInstanceOf[X]))
    }
    None
  }

  def ?[X <: A : TypeTag]: Boolean =
    apply.isDefined

  def foreach[X](f: A => X): Unit = {
    var _list = list
    while (_list.nonEmpty) {
      f(_list.head)
      _list = _list.tail
    }
  }

  def *[X: TypeTag]: Iterable[X] = {
    val tpe = resolveType[X]
    map.filter { case (k, _) => k <:< tpe }.values.asInstanceOf[Iterable[X]]
  }

}
