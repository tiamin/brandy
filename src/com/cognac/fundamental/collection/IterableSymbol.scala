package com.cognac.fundamental.collection

object IterableSymbol {

  type *[A] = Iterable[A]

  val * : Iterable.type = Iterable

}
