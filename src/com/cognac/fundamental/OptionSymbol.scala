package com.cognac.fundamental

object OptionSymbol {

  type ?[A] = Option[A]

  val ? : Option.type = Option

}
