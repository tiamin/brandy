package com.cognac.fundamental.parsing

import com.cognac.fundamental.text.WithTextPosition
import com.cognac.fundamental.text.EscapeExtensions._

final case class Token(label: String, text: String = null) extends WithTextPosition {

  def withLabel(label: String): Token =
    Token(label, text).at(startPosition, endPosition)

  override def toString: String = {
    val b = new StringBuilder
    b.append(positionRangeAsString)
    b.append(label)
    if (text != null) b.append(if (text == label) "." else s" '${ text.escape }'")
    b.toString
  }

}
