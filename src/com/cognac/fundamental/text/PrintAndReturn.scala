package com.cognac.fundamental.text

object PrintAndReturn {

  implicit class PrintAndReturnImpl[A](o: A) {

    def p: A = {
      print(o)
      o
    }

    def p(label: String): A = {
      print(s"$label: $o")
      o
    }

    def P: A = {
      println(o)
      o
    }

    def P(label: String): A = {
      println(s"$label: $o")
      o
    }

  }

}
