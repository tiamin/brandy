package com.cognac.fundamental.text

trait Printer[A] {
  def print(value: A)(implicit indentedPrintWriter: IndentedPrintWriter, textDeco: TextDeco): Unit
}
