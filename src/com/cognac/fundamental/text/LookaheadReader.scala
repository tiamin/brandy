package com.cognac.fundamental.text

import java.io.{ Reader, StringReader }

trait LookaheadReader extends AutoCloseable {

  private var _char: Int = 0
  private var _position: TextPosition = TextPosition.start
  private var _nextPosition: TextPosition = TextPosition.start
  private var _lookahead: Lookahead = _
  private var _freeLookahead: Lookahead = _

  @inline def char: Int = _char
  @inline def position: TextPosition = _position
  @inline def nextPosition: TextPosition = _nextPosition

  @inline def position_=(n: TextPosition): Unit = _position = n

  @inline final def withNext(): this.type = {
    next()
    this
  }

  final def next(): Int = {
    if (_lookahead == null) {
      _char = read()
    } else {
      val la = _lookahead
      _char = la.char
      _lookahead = la._next
      la._next = _freeLookahead
      _freeLookahead = la
    }
    _position = _nextPosition
    _nextPosition = if (_char == '\n') _nextPosition.nextLine else _nextPosition.nextChar
    _char
  }

  @inline private def takeFreeLookahead(): Lookahead = {
    val free = _freeLookahead
    if (free != null) {
      _freeLookahead = free._next
      free._next = null
      free
    } else {
      new Lookahead
    }
  }

  @inline final def matchesLookahead(text: String): Boolean = {
    if (_char == text(0)) {
      var _lookahead = lookahead()
      var i = 1
      while (i < text.length && _lookahead != null && _lookahead.char == text(i)) {
        _lookahead = _lookahead.next()
        i += 1
      }
      i == text.length
    } else false
  }

  @inline final def matchesLookahead(lookahead: Lookahead, text: String): Boolean = {
    var _lookahead = lookahead
    var i = 0
    while (i < text.length && _lookahead != null && _lookahead.char == text(i)) {
      _lookahead = _lookahead.next()
      i += 1
    }
    i == text.length
  }

  @inline final def matchesLookaheadCI(text: String): Boolean = {
    if (_char == text(0)) {
      var _lookahead = lookahead()
      var i = 1
      while (i < text.length &&
        _lookahead != null &&
        _lookahead.char != -1 &&
        _lookahead.char.toChar.toLower == text(i).toLower) {
        _lookahead = _lookahead.next()
        i += 1
      }
      i == text.length
    } else false
  }

  @inline final def matchesLookaheadCI(lookahead: Lookahead, text: String): Boolean = {
    var _lookahead = lookahead
    var i = 0
    while (i < text.length &&
      _lookahead != null &&
      _lookahead.char != -1 &&
      _lookahead.char.toChar.toLower == text(i).toLower) {
      _lookahead = _lookahead.next()
      i += 1
    }
    i == text.length
  }

  @inline final def lookahead(): Lookahead = {
    if (_lookahead != null) _lookahead else {
      val la = takeFreeLookahead()
      la.char = read()
      _lookahead = la
      la
    }
  }

  final def consume(n: Int): Int = {
    var i = 0
    if (i < n) {
      read()
      i += 1
    }
    char
  }

  protected def read(): Int

  class Lookahead {
    var char: Int = _
    var _next: Lookahead = _

    def next(): Lookahead = {
      if (_next == null) {
        val la = takeFreeLookahead()
        la.char = read()
        _next = la
      }
      _next
    }
  }

}

object LookaheadReader {

  def apply(reader: Reader): LookaheadReader = new LookaheadReader {
    private val buf = Array(0.toChar)

    override def read(): Int = {
      val ret = reader.read(buf, 0, 1)
      if (ret == -1) -1 else buf(0)
    }

    override def close(): Unit = reader.close()

  }

  def apply(string: String): LookaheadReader =
    apply(new StringReader(string))

}
