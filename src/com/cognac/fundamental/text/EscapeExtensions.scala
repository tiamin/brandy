package com.cognac.fundamental.text

import com.cognac.fundamental.text.HexLiterals._

object EscapeExtensions {

  implicit class Escapes(val string: String) extends AnyVal {

    def escape: String = {
      val b = new StringBuilder
      string foreach {
        case '\t' => b.append("\\t")
        case '\b' => b.append("\\b")
        case '\n' => b.append("\\n")
        case '\r' => b.append("\\r")
        case '\f' => b.append("\\f")
        case '\'' => b.append("\\\'")
        case '\"' => b.append("\\\"")
        case '\\' => b.append("\\\\")
        case c if c.isControl => b.append(c.toInt.toHex8.substring(4))
        case c => b.append(c)
      }
      b.toString
    }

    def escapeDashes: String =
      string.replace("-", "\\-")

  }

  implicit class Unescapes(val string: String) extends AnyVal {

    def unescape: String = {
      val r = escapeRegex.replaceAllIn(string, { mat =>
        val m = mat.group(1)
        val x = m(0) match {
          case 'u' => m.takeRight(4).hexToInt.toChar.toString
          case 't' => "\t"
          case 'b' => "\b"
          case 'n' => "\n"
          case 'r' => "\r"
          case 'f' => "\f"
          case '\'' => "\'"
          case '\"' => "\""
          case '\\' => "\\\\"
          case c => c.toString
        }
        x
      })
      r
    }

  }

  private val escapeRegex = "\\\\([tbnrf'\"\\\\]|u+[0-9a-fA-F]{4})".r

}
