package com.cognac.fundamental.text

object HexLiterals {

  implicit class IntToHexString(val int: Int) extends AnyVal {

    def toHex: String =
      f"$int%x"

    def toHex8: String =
      f"$int%08x"

  }

  implicit class LongToHexString(val long: Long) extends AnyVal {

    def toHex: String =
      f"$long%x"

    def toHex8: String =
      if (Int.MaxValue.toLong < long || long < Int.MinValue.toLong) toHex16 else long.toInt.toHex8

    def toHex16: String =
      f"$long%016x"

  }

  implicit class HexStringToIntegral(val string: String) extends AnyVal {

    def hexToInt: Int =
      Integer.parseInt(string, 16)

    def hexToLong: Long =
      java.lang.Long.parseLong(string, 16)

  }

}
