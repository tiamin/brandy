package com.cognac.fundamental.text

trait WithTextPosition {
  protected var _startPosition: TextPosition = _
  protected var _endPosition: TextPosition = _

  def startPosition: TextPosition = _startPosition
  def endPosition: TextPosition = _endPosition

  def positionRangeAsString: String = {
    val b = new StringBuilder
    if (startPosition.exists || endPosition.exists) {
      b.append("[")
      if (startPosition.exists) b.append(startPosition)
      b.append("-")
      if (endPosition.exists) b.append(endPosition)
      b.append("] ")
    }
    b.toString
  }

  def at(startPosition: TextPosition, endPosition: TextPosition): this.type = {
    if (!_startPosition.exists) _startPosition = startPosition
    if (!_endPosition.exists) _endPosition = endPosition
    this
  }

  def at(p: WithTextPosition): this.type =
    at(p.startPosition, p.endPosition)

}
