package com.cognac.fundamental.text

trait TextDeco {

  def apply(string: String, deco: String): String
  
  def Black(string: String): String

  def Red(string: String): String

  def Green(string: String): String

  def Yellow(string: String): String

  def Blue(string: String): String

  def Magenta(string: String): String

  def Cyan(string: String): String

  def Gray(string: String): String

  def BrGray(string: String): String

  def BrRed(string: String): String

  def BrGreen(string: String): String

  def BrYellow(string: String): String

  def BrBlue(string: String): String

  def BrMagenta(string: String): String

  def BrCyan(string: String): String

  def White(string: String): String

  def BlackB(string: String): String

  def RedB(string: String): String

  def GreenB(string: String): String

  def YellowB(string: String): String

  def BlueB(string: String): String

  def MagentaB(string: String): String

  def CyanB(string: String): String

  def GrayB(string: String): String

  def BrGrayB(string: String): String

  def BrRedB(string: String): String

  def BrGreenB(string: String): String

  def BrYellowB(string: String): String

  def BrBlueB(string: String): String

  def BrMagentaB(string: String): String

  def BrCyanB(string: String): String

  def WhiteB(string: String): String

  def Reset(string: String): String

  def Bold(string: String): String

  def Underlined(string: String): String

  def Blink(string: String): String

  def Reversed(string: String): String

  def Invisible(string: String): String

}

object TextDeco {

  implicit class TextDecoExtensions(val s: String) extends AnyVal {

    def apply(decoString: String)(implicit deco: TextDeco): String = deco.apply(s, decoString)

    def Black(implicit deco: TextDeco): String = deco.Black(s)

    def Red(implicit deco: TextDeco): String = deco.Red(s)

    def Green(implicit deco: TextDeco): String = deco.Green(s)

    def Yellow(implicit deco: TextDeco): String = deco.Yellow(s)

    def Blue(implicit deco: TextDeco): String = deco.Blue(s)

    def Magenta(implicit deco: TextDeco): String = deco.Magenta(s)

    def Cyan(implicit deco: TextDeco): String = deco.Cyan(s)

    def Gray(implicit deco: TextDeco): String = deco.Gray(s)

    def BrGray(implicit deco: TextDeco): String = deco.BrGray(s)

    def BrRed(implicit deco: TextDeco): String = deco.BrRed(s)

    def BrGreen(implicit deco: TextDeco): String = deco.BrGreen(s)

    def BrYellow(implicit deco: TextDeco): String = deco.BrYellow(s)

    def BrBlue(implicit deco: TextDeco): String = deco.BrBlue(s)

    def BrMagenta(implicit deco: TextDeco): String = deco.BrMagenta(s)

    def BrCyan(implicit deco: TextDeco): String = deco.BrCyan(s)

    def White(implicit deco: TextDeco): String = deco.White(s)

    def BlackB(implicit deco: TextDeco): String = deco.BlackB(s)

    def RedB(implicit deco: TextDeco): String = deco.RedB(s)

    def GreenB(implicit deco: TextDeco): String = deco.GreenB(s)

    def YellowB(implicit deco: TextDeco): String = deco.YellowB(s)

    def BlueB(implicit deco: TextDeco): String = deco.BlueB(s)

    def MagentaB(implicit deco: TextDeco): String = deco.MagentaB(s)

    def CyanB(implicit deco: TextDeco): String = deco.CyanB(s)

    def GrayB(implicit deco: TextDeco): String = deco.GrayB(s)

    def BrGrayB(implicit deco: TextDeco): String = deco.BrGrayB(s)

    def BrRedB(implicit deco: TextDeco): String = deco.BrRedB(s)

    def BrGreenB(implicit deco: TextDeco): String = deco.BrGreenB(s)

    def BrYellowB(implicit deco: TextDeco): String = deco.BrYellowB(s)

    def BrBlueB(implicit deco: TextDeco): String = deco.BrBlueB(s)

    def BrMagentaB(implicit deco: TextDeco): String = deco.BrMagentaB(s)

    def BrCyanB(implicit deco: TextDeco): String = deco.BrCyanB(s)

    def WhiteB(implicit deco: TextDeco): String = deco.WhiteB(s)

    def Reset(implicit deco: TextDeco): String = deco.Reset(s)

    def Bold(implicit deco: TextDeco): String = deco.Bold(s)

    def Underlined(implicit deco: TextDeco): String = deco.Underlined(s)

    def Blink(implicit deco: TextDeco): String = deco.Blink(s)

    def Reversed(implicit deco: TextDeco): String = deco.Reversed(s)

    def Invisible(implicit deco: TextDeco): String = deco.Invisible(s)

  }

  object Console extends TextDeco {

    override def apply(string: String, deco: String): String = deco + string + RESET

    override def Black(string: String): String = BLACK + string + RESET

    override def Red(string: String): String = RED + string + RESET

    override def Green(string: String): String = GREEN + string + RESET

    override def Yellow(string: String): String = YELLOW + string + RESET

    override def Blue(string: String): String = BLUE + string + RESET

    override def Magenta(string: String): String = MAGENTA + string + RESET

    override def Cyan(string: String): String = CYAN + string + RESET

    override def Gray(string: String): String = GRAY + string + RESET

    override def BrGray(string: String): String = BR_GRAY + string + RESET

    override def BrRed(string: String): String = BR_RED + string + RESET

    override def BrGreen(string: String): String = BR_GREEN + string + RESET

    override def BrYellow(string: String): String = BR_YELLOW + string + RESET

    override def BrBlue(string: String): String = BR_BLUE + string + RESET

    override def BrMagenta(string: String): String = BR_MAGENTA + string + RESET

    override def BrCyan(string: String): String = BR_CYAN + string + RESET

    override def White(string: String): String = WHITE + string + RESET

    override def BlackB(string: String): String = BLACK_B + string + RESET

    override def RedB(string: String): String = RED_B + string + RESET

    override def GreenB(string: String): String = GREEN_B + string + RESET

    override def YellowB(string: String): String = YELLOW_B + string + RESET

    override def BlueB(string: String): String = BLUE_B + string + RESET

    override def MagentaB(string: String): String = MAGENTA_B + string + RESET

    override def CyanB(string: String): String = CYAN_B + string + RESET

    override def GrayB(string: String): String = GRAY_B + string + RESET

    override def BrGrayB(string: String): String = BR_GRAY_B + string + RESET

    override def BrRedB(string: String): String = BR_RED_B + string + RESET

    override def BrGreenB(string: String): String = BR_GREEN_B + string + RESET

    override def BrYellowB(string: String): String = BR_YELLOW_B + string + RESET

    override def BrBlueB(string: String): String = BR_BLUE_B + string + RESET

    override def BrMagentaB(string: String): String = BR_MAGENTA_B + string + RESET

    override def BrCyanB(string: String): String = BR_CYAN_B + string + RESET

    override def WhiteB(string: String): String = WHITE_B + string + RESET

    override def Reset(string: String): String = RESET + string + RESET

    override def Bold(string: String): String = BOLD + string + RESET

    override def Underlined(string: String): String = UNDERLINED + string + RESET

    override def Blink(string: String): String = BLINK + string + RESET

    override def Reversed(string: String): String = REVERSED + string + RESET

    override def Invisible(string: String): String = INVISIBLE + string + RESET

    final val BLACK = "\u001b[30m"
    final val RED = "\u001b[31m"
    final val GREEN = "\u001b[32m"
    final val YELLOW = "\u001b[33m"
    final val BLUE = "\u001b[34m"
    final val MAGENTA = "\u001b[35m"
    final val CYAN = "\u001b[36m"
    final val GRAY = "\u001b[37m"
    final val BR_GRAY = "\u001b[90m"
    final val BR_RED = "\u001b[91m"
    final val BR_GREEN = "\u001b[92m"
    final val BR_YELLOW = "\u001b[93m"
    final val BR_BLUE = "\u001b[94m"
    final val BR_MAGENTA = "\u001b[95m"
    final val BR_CYAN = "\u001b[96m"
    final val WHITE = "\u001b[97m"
    final val BLACK_B = "\u001b[40m"
    final val RED_B = "\u001b[41m"
    final val GREEN_B = "\u001b[42m"
    final val YELLOW_B = "\u001b[43m"
    final val BLUE_B = "\u001b[44m"
    final val MAGENTA_B = "\u001b[45m"
    final val CYAN_B = "\u001b[46m"
    final val GRAY_B = "\u001b[47m"
    final val BR_GRAY_B = "\u001b[100m"
    final val BR_RED_B = "\u001b[101m"
    final val BR_GREEN_B = "\u001b[102m"
    final val BR_YELLOW_B = "\u001b[103m"
    final val BR_BLUE_B = "\u001b[104m"
    final val BR_MAGENTA_B = "\u001b[105m"
    final val BR_CYAN_B = "\u001b[106m"
    final val WHITE_B = "\u001b[107m"
    final val RESET = "\u001b[0m"
    final val BOLD = "\u001b[1m"
    final val UNDERLINED = "\u001b[4m"
    final val BLINK = "\u001b[5m"
    final val REVERSED = "\u001b[7m"
    final val INVISIBLE = "\u001b[8m"

  }

  object Off extends TextDeco {

    override def apply(string: String, deco: String): String = string

    def Black(string: String): String = string

    def Red(string: String): String = string

    def Green(string: String): String = string

    def Yellow(string: String): String = string

    def Blue(string: String): String = string

    def Magenta(string: String): String = string

    def Cyan(string: String): String = string

    def Gray(string: String): String = string

    def BrGray(string: String): String = string

    def BrRed(string: String): String = string

    def BrGreen(string: String): String = string

    def BrYellow(string: String): String = string

    def BrBlue(string: String): String = string

    def BrMagenta(string: String): String = string

    def BrCyan(string: String): String = string

    def White(string: String): String = string

    def BlackB(string: String): String = string

    def RedB(string: String): String = string

    def GreenB(string: String): String = string

    def YellowB(string: String): String = string

    def BlueB(string: String): String = string

    def MagentaB(string: String): String = string

    def CyanB(string: String): String = string

    def GrayB(string: String): String = string

    def BrGrayB(string: String): String = string

    def BrRedB(string: String): String = string

    def BrGreenB(string: String): String = string

    def BrYellowB(string: String): String = string

    def BrBlueB(string: String): String = string

    def BrMagentaB(string: String): String = string

    def BrCyanB(string: String): String = string

    def WhiteB(string: String): String = string

    def Reset(string: String): String = string

    def Bold(string: String): String = string

    def Underlined(string: String): String = string

    def Blink(string: String): String = string

    def Reversed(string: String): String = string

    def Invisible(string: String): String = string

  }

}
