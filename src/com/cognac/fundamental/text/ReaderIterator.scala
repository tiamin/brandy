package com.cognac.fundamental.text

import java.io.Reader

class ReaderIterator(reader: Reader) extends Iterator[Char] {
  private var nextChar: Char = _
  private var fetched = false

  override def hasNext: Boolean = {
    if (!fetched) {
      fetched = true
      nextChar = reader.read.toChar
    }
    nextChar != -1.toChar
  }

  override def next(): Char = {
    if (hasNext) fetched = false
    nextChar
  }
}

object ReaderIterator {

  implicit class ReaderToReaderIterator(val reader: Reader) extends AnyVal {
    def toIterator: ReaderIterator = new ReaderIterator(reader)
  }

}
