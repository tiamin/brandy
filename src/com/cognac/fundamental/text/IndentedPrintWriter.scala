package com.cognac.fundamental.text

import java.io.{ PrintWriter, StringWriter, Writer }
import java.util

class IndentedPrintWriter(private val writer: Writer) extends PrintWriter(writer) {
  private val indents = new util.LinkedList[String]
  private var lastNL = true
  private var lastWS = false

  def pushIndent(indent: String): Unit =
    indents.addLast(indent)

  def popIndent(): Unit =
    if (!indents.isEmpty) {
      indents.removeLast()
    }

  def withIndent[U](indent: String)(p: => U): U = {
    pushIndent(indent)
    val result  = p
    popIndent()
    result
  }

  def writeSoftSpace(): Unit = if (!lastWS) {
    write(' ')
  }

  def writeSoftNl(): Unit = if (!lastNL) {
    write('\n')
  }

  override def write(c: Int): Unit = {
    val nl = c == '\n'
    if (lastNL && !nl) {
      indents.forEach(writer.write(_))
    }
    lastNL = nl
    lastWS = c == ' ' || c == '\t'
    writer.write(c)
  }

  override def write(cbuf: Array[Char], off: Int, len: Int): Unit = {
    var i = off
    val e = i + len
    while (i < e) {
      write(cbuf(i))
      i += 1
    }
  }

  override def write(s: String, off: Int, len: Int): Unit = {
    var i = off
    val e = i + len
    while (i < e) {
      write(s(i))
      i += 1
    }
  }

  override def println(): Unit = {
    write('\n')
  }

  override def flush(): Unit =
    writer.flush()

  override def close(): Unit =
    writer.close()

}

object IndentedPrintWriter {

  def apply[A](f: IndentedPrintWriter => A): String = {
    val sw = new StringWriter()
    val pw = new IndentedPrintWriter(sw)
    f(pw)
    val result = sw.toString
    sw.close()
    result
  }

}