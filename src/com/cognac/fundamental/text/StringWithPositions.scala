package com.cognac.fundamental.text

import scala.collection.Searching.{ Found, InsertionPoint }
import scala.language.implicitConversions

case class StringWithPositions(string: String) {
  val lineIndices: IndexedSeq[Int] = string.zipWithIndex.filter(_._1 == '\n').map(_._2 + 1)

  def positionAt(index: Int): TextPosition = lineIndices.search(index) match {
    case Found(lineIndex) =>
      TextPosition(lineIndex + 2, 1)
    case InsertionPoint(0) =>
      TextPosition(1, 1 + index)
    case InsertionPoint(nextLineIndex) =>
      TextPosition(1 + nextLineIndex, index - lineIndices(nextLineIndex - 1) + 1)
  }

}

object StringWithPositions {

  implicit def asString(swp: StringWithPositions): String = swp.string

  implicit class StringExtensionsForStringWithPositions(val string: String) extends AnyVal {
    def withPositions: StringWithPositions = StringWithPositions(string)
  }

}
