package com.cognac.fundamental.text

import scala.util.matching.Regex

object RegexUtil {

  def toRegexLiteral(literal: String): String =
    escapePattern.replaceAllIn(literal, mat => "\\\\\\" + mat.group(0))

  def toRegex(literal: String): Regex =
    toRegexLiteral(literal).r

  private val escapePattern = "[<(\\[{\\\\^\\-=$!|\\]\\})?*+.>]".r

  implicit class RegexUtilsExtensions(val regex: Regex) extends AnyVal {

    def collectAllIn(text: String): Seq[Seq[String]] =
      regex.findAllMatchIn(text).map { mat =>
        (0 to mat.groupCount).map(mat.group)
      }.toSeq

  }

}
