package com.cognac.fundamental.text

import scala.util.matching.Regex

class SeparatorDelimiterParser {

}

object SeparatorDelimiterParser {

  type PrefixMatcher = (String, Int) => Int

  final case class LiteralMatcher(literal: String, ignoreCase: Boolean = false) extends PrefixMatcher {
    private val literalLength = literal.length
    override def apply(text: String, offset: Int): Int =
      if (text.regionMatches(offset, literal, 0, literalLength)) literalLength else -1
  }

  final case class RegexMatcher(regex: Regex) extends PrefixMatcher {
    override def apply(text: String, offset: Int): Int = {
      regex.findPrefixOf(text.subSequence(offset, text.length - 1)) match {
        case Some(subText) => subText.length
        case None => -1
      }
    }
  }

  sealed trait Rule

  sealed trait TextRule

  sealed trait ExecRule

  final case class Matcher(matcher: (String, Int) => Int) extends TextRule

  final case class Separator(
    matcher: (String, Int) => Int) extends TextRule

  final case class Delimiter(
    startMatcher: (String, Int) => Int,
    endMatcher: (String, Int) => Int,
    escape: (String, Int) => Int) extends TextRule


  final case class Match(name: String, start: Int, end: Int, subMatches: Seq[Match]) {
    def asString(implicit source: String): String = source.substring(start, end)
  }

}
