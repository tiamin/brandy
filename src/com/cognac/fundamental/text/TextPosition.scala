package com.cognac.fundamental.text

final case class TextPosition(value: Long) extends AnyVal with Ordered[TextPosition] {

  override def compare(that: TextPosition): Int =
    this.value.compareTo(that.value)

  @inline def exists: Boolean =
    value != 0L

  @inline def lineNo: Int =
    (value >>> 32).toInt

  @inline def charNo: Int =
    (value & 0xffffffff).toInt

  @inline def nextChar: TextPosition =
    TextPosition(value + 1L)

  @inline def nextLine: TextPosition =
    TextPosition(lineNo + 1, 1)

  @inline def next(char: Char): TextPosition = char match {
    case '\r' => TextPosition(lineNo, 1)
    case '\n' => nextLine
    case _ => nextChar
  }

  override def toString: String =
    s"$lineNo:$charNo"

}

object TextPosition {

  val empty: TextPosition = TextPosition(0)

  val start: TextPosition = TextPosition(1, 1)

  def apply(lineNo: Int, charNo: Int): TextPosition =
    TextPosition((lineNo.toLong << 32) | charNo.toLong)

}
