package com.cognac.fundamental.text

import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets

object ReadTextResource {
  def apply(path: String): String = {
    val stream = getClass.getResourceAsStream(path)
    if (stream != null) try {
      val bytes = stream.readAllBytes()
      new String(bytes, StandardCharsets.UTF_8)
    } finally {
      stream.close()
    } else throw new FileNotFoundException(path)
  }
}
