package com.cognac.fundamental.weights

object WithWeightSymbol {

  type %[A] = WithWeight[A]
  val % : WithWeight.type = WithWeight

}
