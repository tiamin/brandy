package com.cognac.fundamental.weights

trait ImplicitWeightedFunction[A, B] extends ((A, B) => Weight)

object ImplicitWeightedFunction {
  type =>%[A, B] = ImplicitWeightedFunction[A, B]
}
