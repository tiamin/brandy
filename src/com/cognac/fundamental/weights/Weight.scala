package com.cognac.fundamental.weights

import scala.language.implicitConversions

final case class Weight(value: Double) extends AnyVal with Ordered[Weight] {
  
  def +(that: Weight): Weight = Weight(this.value + that.value)
  def -(that: Weight): Weight = Weight(this.value - that.value)
  def *(that: Weight): Weight = Weight(this.value * that.value)
  def /(that: Weight): Weight = Weight(this.value / that.value)
  
  def +~(that: Weight): Weight = ~Weight(this.value + that.value)
  def -~(that: Weight): Weight = ~Weight(this.value - that.value)
  def *~(that: Weight): Weight = ~Weight(this.value * that.value)
  def /~(that: Weight): Weight = ~Weight(this.value / that.value)

  def +!(that: Weight): Weight = !Weight(this.value + that.value)
  def -!(that: Weight): Weight = !Weight(this.value - that.value)
  def *!(that: Weight): Weight = !Weight(this.value * that.value)
  def /!(that: Weight): Weight = !Weight(this.value / that.value)

  def unary_~ : Weight = Weight.norm(value)
  def unary_! : Weight = Weight.clamp(value)

  def min(that: Weight): Weight = Weight(this.value min that.value)
  def max(that: Weight): Weight = Weight(this.value max that.value)
  def avg(that: Weight): Weight = Weight((this.value + that.value) / 2.0)

  def &(that: Weight): Weight = Weight(1 - (1 - this.value) * (1 - that.value))
  
  override def compare(that: Weight): Int = java.lang.Double.compare(this.value, that.value)
  override def toString: String = value.toString
}

object Weight {
  def norm(value: Double): Weight = Weight(1 / (1 - Math.exp(-value)))
  def clamp(value: Double): Weight = Weight(if (value < 0.0) 0.0 else if (1.0 < value) 1.0 else value)

  implicit def weightToDouble(weight: Weight): Double = weight.value
  implicit def doubleToWeight(double: Double): Weight = Weight(double)

}
