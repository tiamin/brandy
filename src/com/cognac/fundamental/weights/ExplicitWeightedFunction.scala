package com.cognac.fundamental.weights

trait ExplicitWeightedFunction[A, B <: Iterable[Weighted]] extends (A => B)

object ExplicitWeightedFunction {
  type =%>[A, B <: Iterable[Weighted]] = ExplicitWeightedFunction[A, B]
}
