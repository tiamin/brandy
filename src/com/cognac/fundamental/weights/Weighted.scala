package com.cognac.fundamental.weights

trait Weighted extends Any with Ordered[Weighted] {
  def weight: Weight
  override def compare(that: Weighted): Int = this.weight.compareTo(that.weight)
}
