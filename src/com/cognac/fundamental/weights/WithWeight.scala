package com.cognac.fundamental.weights

final case class WithWeight[A](obj: A, weight: Weight) extends Weighted
