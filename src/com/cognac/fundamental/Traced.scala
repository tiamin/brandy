package com.cognac.fundamental

import java.util.Objects
import scala.util.{ Failure, Success, Try }

trait Traced[+A] {
  def symbol: String
  def args: Seq[Traced[_]]
  def result: Try[A]
  override def toString: String = s"$symbol(${ args.mkString(", ") })=$resultString"
  def resultString: String = result match {
    case Success(value) => s"$value"
    case Failure(ex) => s"!{${ ex.getMessage }}"
  }
}

object Traced {

  object NonExecuted extends Traced[Nothing] {
    override def symbol: String = null
    override def args: Seq[Traced[_]] = Seq.empty
    override def result: Try[Nothing] = throw new RuntimeException("non executed")
    override def toString: String = "#"
    override def resultString: String = "#"
  }

  implicit class TracedExtensions[A](val self: Traced[A]) extends AnyVal {
    def ===(that: Traced[A])(implicit n: Equiv[A]): Traced[Boolean] = f2("===", n.equiv)(self, that)
    def !==(that: Traced[A])(implicit n: Equiv[A]): Traced[Boolean] = f2("===", !n.equiv(_: A, _: A))(self, that)
    def +(that: Traced[A])(implicit n: Numeric[A]): Traced[A] = f2("+", n.plus)(self, that)
    def -(that: Traced[A])(implicit n: Numeric[A]): Traced[A] = f2("-", n.minus)(self, that)
    def *(that: Traced[A])(implicit n: Numeric[A]): Traced[A] = f2("*", n.times)(self, that)
    def /(that: Traced[A])(implicit n: Fractional[A]): Traced[A] = f2("/", n.div)(self, that)
    def /%(that: Traced[A])(implicit n: Integral[A]): Traced[A] = f2("/%", n.quot)(self, that)
    def <(that: Traced[A])(implicit n: Ordering[A]): Traced[Boolean] = f2("<", n.lt)(self, that)
    def <=(that: Traced[A])(implicit n: Ordering[A]): Traced[Boolean] = f2("<=", n.lteq)(self, that)
    def >(that: Traced[A])(implicit n: Ordering[A]): Traced[Boolean] = f2(">", n.gt)(self, that)
    def >=(that: Traced[A])(implicit n: Ordering[A]): Traced[Boolean] = f2(">=", n.gteq)(self, that)
    def unary_-(implicit n: Numeric[A]): Traced[A] = f1("-", n.negate)(self)
  }

  implicit class TracedStringExtensions(val self: Traced[String]) extends AnyVal {
    def +(that: Traced[String]): Traced[String] = f2("+", (_: String) + (_: String))(self, that)
    def *(that: Traced[Int]): Traced[String] = f2("*", (_: String) * (_: Int))(self, that)
  }

  implicit class TracedBooleanExtensions(val self: Traced[Boolean]) extends AnyVal {
    def &&(that: Traced[Boolean]): Traced[Boolean] = f2sc("&&", (_: Boolean) && (_: () => Boolean) ())(self, that)
    def ||(that: Traced[Boolean]): Traced[Boolean] = f2sc("||", (_: Boolean) || (_: () => Boolean) ())(self, that)
    def ^(that: Traced[Boolean]): Traced[Boolean] = f2sc("^", (_: Boolean) ^ (_: () => Boolean) ())(self, that)
    def unary_! : Traced[Boolean] = f1("!", !(_: Boolean))(self)
  }

  implicit class NameOperatorExtension(val name: String) extends AnyVal {
    def :=[A](that: Traced[A]): Traced[A] = new Traced[A] {
      override def symbol: String = name
      override def args: Seq[Traced[_]] = that.args
      override def result: Try[A] = that.result
      override def toString: String = s"$name:$that"
    }
  }

  implicit class F0ExtensionsForTraced[A](val f: () => A) extends AnyVal {
    def withSymbol(symbol: String): () => A = new (() => A) {
      override def apply(): A = f()
      override def toString: String = symbol
    }
    def apply(): Traced[A] = f0(f.toString, f)
  }

  implicit class F1ExtensionsForTraced[A, B](val f: A => B) extends AnyVal {
    def withSymbol(symbol: String): A => B = new (A => B) {
      override def apply(v: A): B = f(v)
      override def toString: String = symbol
    }
    def apply(a1: Traced[A]): Traced[B] = f1(f.toString, f)(a1)
  }

  implicit class F2ExtensionsForTraced[A, B, C](val f: (A, B) => C) extends AnyVal {
    def withSymbol(symbol: String): (A, B) => C = new ((A, B) => C) {
      override def apply(v1: A, v2: B): C = f(v1, v2)
      override def toString: String = symbol
    }
    def apply(a1: Traced[A], a2: Traced[B]): Traced[C] = f2(f.toString, f)(a1, a2)
  }

  implicit class F0TracedExtensions[A](val obj: Traced[_ <: () => A]) extends AnyVal {
    def apply(): Traced[A] = f0(obj.toString, obj.result.get)
  }

  implicit class F1TracedExtensions[A, B](val obj: Traced[_ <: A => B]) extends AnyVal {
    def apply(a: Traced[A]): Traced[B] = f1(obj.toString, obj.result.get)(a)
  }

  implicit class F2TracedExtensions[A, B, C](val obj: Traced[_ <: (A, B) => C]) extends AnyVal {
    def apply(a: Traced[A], b: Traced[B]): Traced[C] = f2(obj.toString, obj.result.get)(a, b)
  }

  def const[A](value: A): Traced[A] = new Traced[A] {
    override def symbol: String = null
    override def args: Seq[Traced[_]] = Seq.empty
    override val result: Try[A] = Success(value)
    override def toString: String = Objects.toString(value)
  }

  def If[A](_a1: Traced[Boolean])(__a2: => Traced[A]): IfThen[A] = new IfThen[A](_a1, __a2)

  class IfThen[A](val _a1: Traced[Boolean], __a2: => Traced[A]) extends Traced[Any] {
    override def symbol: String = "__if__"
    private var _a2: Traced[A] = NonExecuted
    override val result: Try[Any] = Try(if (_a1.result.get) { _a2 = __a2; _a2.result })
    override def args: Seq[Traced[_]] = Seq(_a1, _a2)
    override def toString: String = s"(If(${ _a1 }) ${ _a2 })=$resultString"
    def Else[B](__a3: => Traced[B])(implicit t: CommonSupertype[A, B]): Traced[t.T] = new Traced[t.T] {
      override def symbol: String = "__if_else__"
      private var _a2: Traced[A] = NonExecuted
      private var _a3: Traced[B] = NonExecuted
      override val result: Try[t.T] =
        Try(if (_a1.result.get) { _a2 = __a2; _a2.result.get } else { _a3 = __a3; _a3.result.get })
          .asInstanceOf[Try[t.T]]
      override def args: Seq[Traced[_]] = Seq(_a1, _a2, _a3)
      override def toString: String = s"(If(${ _a1 }) ${ _a2 } Else ${ _a3 })=$resultString"
    }
  }

  private def f0[A](_symbol: String, f: () => A): Traced[A] = new Traced[A] {
    override def symbol: String = _symbol
    override def args: Seq[Traced[_]] = Seq.empty
    override val result: Try[A] = Try(f())
  }

  private def f1[A, B](_symbol: String, f: A => B)
    (a1: Traced[A]): Traced[B] = new Traced[B] {
    override def symbol: String = _symbol
    override def args: Seq[Traced[_]] = Seq(a1)
    override val result: Try[B] = Try(f(a1.result.get))
  }

  private def f2[A, B, C](_symbol: String, f: (A, B) => C)
    (a1: Traced[A], a2: => Traced[B]): Traced[C] = new Traced[C] {
    override def symbol: String = _symbol
    override def args: Seq[Traced[_]] = Seq(a1, _a2)
    private var _a2: Traced[B] = NonExecuted
    override val result: Try[C] = Try(f(a1.result.get, { _a2 = a2; _a2.result.get }))
  }

  private def f2sc[A, B, C](_symbol: String, f: (A, () => B) => C)
    (a1: Traced[A], a2: => Traced[B]): Traced[C] = new Traced[C] {
    override def symbol: String = _symbol
    override def args: Seq[Traced[_]] = Seq(a1, _a2)
    private var _a2: Traced[B] = NonExecuted
    override val result: Try[C] = Try(f(a1.result.get, () => { _a2 = a2; _a2.result.get }))
  }

  private type -[A] = A => Nothing
  private type |[T, U] = -[-[T] with -[U]]
  private type --[A] = -[-[A]]

  trait CommonSupertype[A, B] {
    type T
  }

  implicit def commonSupertype[A, B, C](implicit C: (A | B) <:< --[C]): CommonSupertype[A, B] {type T = C} =
    new CommonSupertype[A, B] {
      type T = C
    }

}
