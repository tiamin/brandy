package com.cognac.fundamental

import com.cognac.fundamental.Traced._

object Examples {

  def main(args: Array[String]): Unit = {
    example1(2.0, 3.0, 1.0)
    example1(6.0, 5.0, 4.0)
  }

  private def example1(_a: Double, _b: Double, _c: Double): Unit = {
    val a = "a" := const(_a)
    val b = "b" := const(_b)
    val c = "c" := const(_c)
    val sqrt = { x: Double =>
      if (0.0 <= x) Math.sqrt(x)
      else throw new IllegalArgumentException("sqrt:negative arg")
    }.withSymbol("sqrt")
    val d = sqrt(b * b - const(4.0) * a * c)
    val a2 = const(2.0) * a
    val solution1 = (-b - d) / a2
    val solution2 = (-b + d) / a2
    println(solution1)
    println(show(solution1))
    println(solution2)
    println(show(solution2))
  }

  def show(t: Traced[_]): String = {
    val NL = "\n"
    val pad: String => String = _.linesIterator.map(". " + _).mkString(NL)
    val sb = new StringBuilder
    if (t.symbol != null) sb.append(s"[${ t.symbol }]").append(" = ")
    sb.append(t.resultString).append(NL)
    t.args.foreach { a => sb.append(pad(show(a))).append(NL) }
    sb.toString
  }

}
