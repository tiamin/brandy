package com.cognac.fundamental

import scala.reflect.runtime.universe._

object TypeUtils {

  def dealias(tpe: Type): Type = {
    tpe.toString // (!) needed to expand inner structure
    val _tpe = tpe.dealias
    val _args = _tpe.typeArgs map dealias
    val genericType = _tpe.typeSymbol.asType.toType
    genericType.substituteSymbols(genericType.typeArgs.map(_.typeSymbol), _args.map(_.typeSymbol))
  }

  @inline def resolveType[X: TypeTag]: Type =
    dealias(implicitly[TypeTag[X]].tpe)

  @inline def baseTypes[X: TypeTag]: List[Type] = {
    val tpe = resolveType[X]
    tpe.baseClasses map tpe.baseType
  }

}
