package com.cognac.fundamental.namespace

trait Namespace[N, A] extends (N => A) with Iterable[(N, A)] {

  def nameIterator: Iterator[N]

  def apply(name: N): A

  def contains(name: N): Boolean

  def getOrNull(name: N): A

  def add(name: N, member: A): Unit

  def remove(name: N): Boolean

  def clear(): Boolean

}
