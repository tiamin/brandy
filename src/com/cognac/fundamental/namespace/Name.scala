package com.cognac.fundamental.namespace

trait Name extends Any {
  def isEmpty: Boolean
  def +(that: Name): Name
}

object Name {

  def apply(string: String): Name = {
    if (string.isEmpty) EmptyName else if (string.contains(".")) FullName(string) else SimpleName(string)
  }

  object HeadTail {
    def unapply(name: Name): Option[(SimpleName, Name)] = name match {
      case FullName(parts) =>
        parts.length match {
          case 0 => None
          case 1 => None
          case 2 => Some((parts.head, parts.last))
          case _ => Some((parts.head, FullName(parts.tail)))
        }
      case _ =>
        None
    }
  }

  object InitLast {
    def unapply(name: Name): Option[(Name, SimpleName)] = name match {
      case FullName(parts) =>
        parts.length match {
          case 0 => None
          case 1 => None
          case 2 => Some((parts.head, parts.last))
          case _ => Some((FullName(parts.init), parts.last))
        }
      case _ =>
        None
    }
  }

}

case object EmptyName extends Name {
  override def isEmpty: Boolean = true
  override def +(that: Name): Name = that
  override def toString: String = ""
}

final case class SimpleName(literal: String) extends Name {
  require(literal.nonEmpty, "empty single name")
  require(!literal.contains("."), s"'$literal' is not a single name")

  override def isEmpty: Boolean = false

  override def +(that: Name): Name = that match {
    case EmptyName => this
    case s2 @ SimpleName(_) => FullName(Seq(this, s2))
    case FullName(parts) => FullName(this +: parts)
  }

  override def toString: String =
    literal

}

final case class FullName(parts: Seq[SimpleName]) extends Name with Seq[SimpleName] {
  require(parts.length > 1)

  override def isEmpty: Boolean = false
  override def apply(i: Int): SimpleName = parts(i)
  override def length: Int = parts.length
  override def iterator: Iterator[SimpleName] = parts.iterator
  override def +(that: Name): Name = that match {
    case EmptyName => this
    case s2 @ SimpleName(_) => FullName(parts :+ s2)
    case FullName(_parts) => FullName(parts ++ _parts)
  }
  override def toString: String =
    parts.mkString(".")
}

object FullName {

  def apply(string: String): FullName =
    FullName(string.split("\\.").toSeq.map(SimpleName))

}
