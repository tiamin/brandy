package com.cognac.fundamental.namespace

import java.util.NoSuchElementException

class TreeNamespace[A] extends Namespace[Name, A] {
  self =>
  private var _hasValue: Boolean = false
  private var _value: A = _
  val namespace = new FlatNamespace[TreeNamespace[A]]

  override def apply(name: Name): A = name match {
    case EmptyName =>
      if (_hasValue) _value else throw new NoSuchElementException(name.toString)
    case s @ SimpleName(_) =>
      namespace(s)(EmptyName)
    case f @ FullName(_) =>
      try {
        val Name.HeadTail(head, tail) = f
        val subTree = namespace(head)
        subTree(tail)
      } catch {
        case _: NoSuchElementException =>
          throw new NoSuchElementException(f.toString)
      }
  }

  override def contains(name: Name): Boolean = name match {
    case EmptyName =>
      _hasValue
    case s @ SimpleName(_) =>
      if (namespace.contains(s)) namespace.contains(s) else false
    case f @ FullName(_) =>
      val Name.HeadTail(head, tail) = f
      if (namespace.contains(head)) {
        val subTree = namespace(head)
        subTree.contains(tail)
      } else false
  }

  override def getOrNull(name: Name): A = name match {
    case EmptyName =>
      if (_hasValue) _value else null.asInstanceOf[A]
    case s @ SimpleName(_) =>
      if (namespace.contains(s)) namespace(s)(EmptyName) else null.asInstanceOf[A]
    case f @ FullName(_) =>
      val Name.HeadTail(head, tail) = f
      if (namespace.contains(head)) {
        namespace(head)(tail)
      } else null.asInstanceOf[A]
  }

  override def add(name: Name, member: A): Unit = name match {
    case EmptyName =>
      _value = member
      _hasValue = true
    case s @ SimpleName(_) =>
      if (namespace.contains(s)) namespace(s).add(EmptyName, member) else {
        val subTree = new TreeNamespace[A]
        namespace.add(s, subTree)
        subTree.add(EmptyName, member)
      }
    case f @ FullName(_) =>
      val Name.HeadTail(head, tail) = f
      if (namespace.contains(head)) {
        namespace(head).add(tail, member)
      } else {
        val subTree = new TreeNamespace[A]
        namespace.add(head, subTree)
        subTree.add(tail, member)
      }
  }

  override def remove(name: Name): Boolean = name match {
    case EmptyName =>
      if (_hasValue) {
        _value = null.asInstanceOf[A]
        _hasValue = false
        namespace.nonEmpty
      } else false
    case s @ SimpleName(_) =>
      if (namespace.contains(s)) {
        if (namespace(s).remove(EmptyName)) {
          namespace.remove(s)
        } else false
      } else false
    case f @ FullName(_) =>
      val Name.HeadTail(head, tail) = f
      if (namespace.contains(head)) {
        if (namespace(head).remove(tail)) namespace.remove(head) else false
      } else false
  }

  override def clear(): Boolean = ???

  override def nameIterator: Iterator[Name] =
    iterator.map(_._1)

  override def iterator: Iterator[(Name, A)] = new Iterator[(Name, A)] {
    private var _currentName: Name = _
    private var _currentTree: TreeNamespace[A] = _
    private var _names: List[Name] = EmptyName :: Nil
    private var _iterators: List[Iterator[(SimpleName, TreeNamespace[A])]] = Iterator((null, self)) :: Nil
    step()

    override def hasNext: Boolean =
      _currentTree != null

    override def next(): (Name, A) = {
      val v = nullToEmpty(_currentName) -> _currentTree._value
      step()
      v
    }

    private def step(): Unit = {
      _currentTree = null
      while (_currentTree == null && _iterators.nonEmpty) {
        while (_currentTree == null && _iterators.head.hasNext) {
          val (_name, _node) = _iterators.head.next()
          _currentName = _names.head + nullToEmpty(_name)
          if (_node._hasValue) { _currentTree = _node }
          _names = _currentName :: _names
          _iterators = _node.namespace.iterator :: _iterators
        }
        if (_currentTree == null) {
          _names = _names.tail
          _iterators = _iterators.tail
        }
      }
    }

    @inline private def nullToEmpty(s: Name): Name = if (s == null) EmptyName else s

  }

  override def toString: String = s"TreeNamespace(${ namespace.nameIterator.mkString(", ") })"

}
