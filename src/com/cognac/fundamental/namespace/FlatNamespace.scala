package com.cognac.fundamental.namespace

import com.cognac.fundamental.ListenerFunctions
import com.cognac.fundamental.collection.DLNodeList

import java.util.{ HashMap => JHashMap }

class FlatNamespace[A] extends Namespace[SimpleName, A] {

  val onAdd: ListenerFunctions[(SimpleName, A)] = ListenerFunctions()
  val onUpdate: ListenerFunctions[(SimpleName, A, A)] = ListenerFunctions()
  val onRemove: ListenerFunctions[(SimpleName, A)] = ListenerFunctions()

  private val memberList: DLNodeList[(SimpleName, A)] =
    DLNodeList.empty[(SimpleName, A)]

  private val memberMap: JHashMap[SimpleName, DLNodeList.Node[(SimpleName, A)]] =
    new JHashMap[SimpleName, DLNodeList.Node[(SimpleName, A)]]

  override def nameIterator: Iterator[SimpleName] =
    memberList.iterator.map(_._1)

  def apply(name: SimpleName): A = {
    val node = memberMap.get(name)
    if (node != null) node.value._2 else throw new NoSuchElementException(name.toString)
  }

  override def size: Int = memberList.size

  override def contains(name: SimpleName): Boolean =
    memberMap.containsKey(name)

  def getOrNull(name: SimpleName): A = {
    val node = memberMap.get(name)
    if (node != null) node.value._2 else null.asInstanceOf[A]
  }

  def add(name: SimpleName, member: A): Unit = memberMap.get(name) match {

    case null =>
      val entry = (name, member)
      val node = new DLNodeList.Node[(SimpleName, A)](entry)
      memberList.insertLast(node)
      memberMap.put(name, node)
      onAdd.apply(entry)

    case oldNode =>
      val newNode = new DLNodeList.Node[(SimpleName, A)](name -> member)
      val oldMember = oldNode.value
      oldNode.insertNext(newNode)
      oldNode.remove()
      memberMap.put(name, newNode)
      onUpdate((name, oldMember._2, member))

  }

  def remove(name: SimpleName): Boolean = memberMap.get(name) match {
    case null =>
      false
    case node =>
      val member = node.value
      node.remove()
      memberMap.remove(name)
      onRemove(name -> member._2)
      true
  }

  import scala.jdk.CollectionConverters._
  def clear(): Boolean = {
    val result = !memberMap.isEmpty
    val entries = memberMap.entrySet().asScala.toSeq
    memberList.clear()
    memberMap.clear()
    entries.foreach { entry => onRemove(entry.getKey -> entry.getValue.value._2)  }
    result
  }

  override def iterator: Iterator[(SimpleName, A)] =
    memberList.iterator

}
