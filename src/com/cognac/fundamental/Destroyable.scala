package com.cognac.fundamental

trait Destroyable {
  def destroy(): Unit
}
