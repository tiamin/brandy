package com.cognac.fundamental

import scala.annotation.tailrec

class ListenerFunctions[A] extends (A => Unit) {
  private var listeners: List[A => _] = Nil

  def add[X](listener: A => X): Unit =
    listeners = listener :: listeners

  def remove[X](listener: A => X): Unit =
    listeners = listeners.filterNot(listener == _)

  def clear(): Unit =
    listeners = Nil

  override def apply(value: A): Unit =
    _apply(value, listeners)

  @tailrec
  private def _apply(value: A, listeners: List[A => _]): Unit = listeners match {
    case listener :: otherListeners =>
      listener(value)
      _apply(value, otherListeners)
    case Nil =>
  }

  def +=[X](f: A => X): this.type = {
    add(f)
    this
  }

}

object ListenerFunctions {
  def apply[A](): ListenerFunctions[A] = new ListenerFunctions[A]
}
