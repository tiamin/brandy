package com.cognac.fundamental

trait FluentApi {

  @inline protected def fluent[A](p: => A): this.type = { p; this }

  @inline def fluent[A](f: this.type => A): this.type = { f(this); this }

}
