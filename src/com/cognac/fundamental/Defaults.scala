package com.cognac.fundamental

object Defaults {

  @inline def default[A]: A =
    null.asInstanceOf[A]

}
