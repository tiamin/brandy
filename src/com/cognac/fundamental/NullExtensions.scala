package com.cognac.fundamental

object NullExtensions {

  implicit class NullHelpers[A >: Null](val value: A) extends AnyVal {
    def ??(default: A): A = if (value == null) default else value
  }

}
