package com.cognac.fundamental

final class Nullable[+A >: Null](val value: A) extends AnyVal with Product with Serializable {
  import Nullable._

  @inline def isEmpty: Boolean =
    value == null

  @inline def isDefined: Boolean =
    value != null

  @inline def get: A =
    if (isDefined) value else throw new NoSuchElementException("non-empty")

  @inline def getOrElse[B >: A](default: => B): B =
    if (isDefined) value else default

  @inline def orNull: A =
    value

  @inline def map[B >: Null](f: A => B): Nullable[B] =
    if (isDefined) Nullable(f(value)) else Null

  @inline def fold[B >: Null](ifEmpty: => B)(f: A => B): B =
    if (isEmpty) ifEmpty else f(this.get)

  @inline def flatMap[B >: Null](f: A => Nullable[B]): Nullable[B] =
    if (isEmpty) Null else f(this.get)

  @inline def flatten[B >: Null](implicit ev: A <:< Nullable[B]): Nullable[B] =
    this.asInstanceOf[Nullable[B]]

  @inline def filter(p: A => Boolean): Nullable[A] =
    if (isEmpty || p(this.get)) this else Null

  @inline def filterNot(p: A => Boolean): Nullable[A] =
    if (isEmpty || !p(this.get)) this else Null

  @inline def nonEmpty: Boolean =
    isDefined

  @inline def contains[A1 >: A](elem: A1): Boolean =
    isDefined && value == elem

  @inline def exists(p: A => Boolean): Boolean =
    isDefined && p(this.get)

  @inline def forall(p: A => Boolean): Boolean =
    isEmpty || p(this.get)

  @inline def foreach[U](f: A => U): Unit =
    if (isDefined) f(this.get)

  @inline def collect[B](pf: PartialFunction[A, B]): Option[B] =
    if (isDefined) pf.lift(this.get) else None

  @inline def orElse[B >: A](alternative: => Nullable[B]): Nullable[B] =
    if (isEmpty) alternative else this

  def iterator: Iterator[A] =
    if (isEmpty) Iterator.empty else Iterator.single(value)

  def toList: List[A] =
    if (isEmpty) List() else new ::(value, Nil)

  @inline def toRight[X](left: => X): Either[X, A] =
    if (isEmpty) Left(left) else Right(value)

  @inline def toLeft[X](right: => X): Either[A, X] =
    if (isEmpty) Right(right) else Left(value)

  override def productElement(n: Int): Any =
    if (nonEmpty && n == 0) value else throw new IndexOutOfBoundsException

  override def productArity: Int =
    if (nonEmpty) 1 else 0

  override def canEqual(that: Any): Boolean =
    true
}

object Nullable {

  val Null: Nullable[Null] =
    Nullable(null)

  def apply[A >: Null](value: A): Nullable[A] =
    new Nullable[A](value)

  def unapply[A >: Null](value: Nullable[A]): Option[A] =
    if (value.isEmpty) None else Some(value.value)

  def unapplySeq[A >: Null](value: Nullable[A]): Seq[A] =
    if (value.isEmpty) Seq.empty else Seq(value.value)

}
