package com.cognac.engine.struct

import com.cognac.fundamental.text.EscapeExtensions.Escapes

import java.util.Objects
import scala.reflect.ClassTag.Null

trait Node {
  def parentId: NodeId
  def withParent(parentId: NodeId): Node
  @inline def hasParent: Boolean = parentId != NodeId.Undefined
  @inline def hasNoParent: Boolean = parentId == NodeId.Undefined
}

object Node {

  final case class Ref(parentId: NodeId, entityId: EntityId) extends Node {
    override def withParent(parentId: NodeId): Node = copy(parentId = parentId)
    override def toString: String = entityId.toString
  }

  final case class Val(parentId: NodeId, value: AnyRef) extends Node {
    override def withParent(parentId: NodeId): Val = copy(parentId = parentId)
    override def toString: String = {
      val clazz = if (value != null) value.getClass else Null.runtimeClass
      val clazzName = clazz.getSimpleName
      val valueString = Objects.toString(value).escape
      s"$clazzName[$valueString]"
    }
  }

  final case class App(parentId: NodeId, baseId: NodeId, rawArgs: Array[NodeId]) extends Node {

    def argCount: Int = rawArgs.length / 2

    def arg(index: Int): NodeId = rawArgs(index * 2)

    def role(index: Int): NodeId = rawArgs(index * 2 + 1)

    def foreachArg[U](f: (NodeId, NodeId) => U): Unit = {
      var i = 0
      while (i < rawArgs.length) {
        f(rawArgs(i), rawArgs(i + 1))
        i += 2
      }
    }

    def argIterator: Iterator[(NodeId, NodeId)] = new Iterator[(NodeId, NodeId)] {
      private var index = 0
      override def hasNext: Boolean = index < rawArgs.length
      override def next(): (NodeId, NodeId) = {
        val entry = (rawArgs(index), rawArgs(index + 1))
        index += 2
        entry
      }
    }

    override def withParent(parentId: NodeId): App = copy(parentId = parentId)

    def withBase(baseId: NodeId): App = copy(baseId = baseId)

    def withArg(argId: NodeId, roleId: NodeId): App = {
      val l = rawArgs.length
      val i = argIndex(argId)
      if (i < 0) {
        val newArgs = new Array[NodeId](l + 2)
        System.arraycopy(rawArgs, 0, newArgs, 0, l)
        newArgs(l) = argId
        newArgs(l + 1) = roleId
        copy(rawArgs = newArgs)
      } else {
        val r = rawArgs(i + 1)
        if (r == roleId) this else {
          val newArgs = new Array[NodeId](l)
          System.arraycopy(rawArgs, 0, newArgs, 0, l)
          newArgs(i) = argId
          newArgs(i + 1) = roleId
          copy(rawArgs = newArgs)
        }
      }
    }

    def withoutArg(argId: NodeId): App = {
      val i = argIndex(argId)
      if (i < 0) this else withoutArg(i)
    }

    def withoutArg(index: Int): App = {
      val l = rawArgs.length
      val newArgs = new Array[NodeId](l - 2)
      if (0 < index) System.arraycopy(rawArgs, 0, newArgs, 0, index)
      if (index < l - 2) System.arraycopy(rawArgs, index + 2, newArgs, index, l - index - 2)
      copy(rawArgs = newArgs)
    }

    def argIndex(argId: NodeId): Int = {
      val len = rawArgs.length
      var i = 0
      var n = -1
      while (i < len) {
        val arg = rawArgs(i)
        if (arg == argId) { n = i; i = len } else { i += 2 }
      }
      n
    }

    def roleIndex(roleId: NodeId): Int = {
      val len = rawArgs.length
      var i = 0
      var n = -1
      while (i < len) {
        val role = rawArgs(i + 1)
        if (role == roleId) { n = i; i = len } else { i += 2 }
      }
      n
    }

    override def toString: String =
      s"$baseId(${ argIterator.map(e => s"${e._1}: ${e._2}").mkString(", ") })"

  }

}
