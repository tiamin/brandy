package com.cognac.engine.struct

import com.cognac.fundamental.collection.IterableSymbol.*
import scala.reflect.runtime.universe._

trait Plane {
  def nodes: *[NodeId]
  def apply(nodeId: NodeId): Node
  def addEntity(entityType: EntityType): EntityId
  def addRef(entityId: EntityId): NodeId
  def addVal(value: AnyRef): NodeId
  def addApp(): NodeId
  def link(appId: NodeId, baseId: NodeId): Unit
  def link(appId: NodeId, argId: NodeId, roleId: NodeId): Unit
  def unlink(nodeId: NodeId): Unit
  def remove(nodeId: NodeId): Unit
  def copy: Plane

  def load(exp: Exp): NodeId = exp match {
    case Exp.Ref(entityId) => addRef(entityId)
    case Exp.Val(value) => addVal(value)
    case Exp.App(base, args) =>
      val appId = addApp()
      link(appId, load(base))
      args.foreach { case (arg, role) => link(appId, load(arg), load(role)) }
      appId
  }

  protected var _plugins: List[(String, Type, Plugin)] = Nil

  def plugins: *[(String, Type, Plugin)] = _plugins

  def apply(pluginConstructor: PluginConstructor): pluginConstructor.pluginType = {
    val id = pluginConstructor.id
    plugins.find { case (_id, _tpe, _) =>
      id == _id && _tpe <:< pluginConstructor.pluginTpe
    }.map(_._3).orNull.asInstanceOf[pluginConstructor.pluginType]
  }

  def install(pluginConstructor: PluginConstructor): pluginConstructor.pluginType =
    apply(pluginConstructor) match {
      case null =>
        val plugin = pluginConstructor.apply(this)
        val pluginTpe = pluginConstructor.pluginTpe
        _plugins = (pluginConstructor.id, pluginTpe, plugin) +: _plugins
        plugin.initialize()
        plugin
      case plugin => plugin
    }

}
