package com.cognac.engine.struct

import scala.collection.immutable.LongMap

class MatchIndex private (private val _plane: Plane) extends Plugin {

  private var _indices = Vector[Traversable](All)

  override def plane: Plane = _plane

  override protected [struct] def initialize(): Unit = {

  }

  def query[U](exp: Exp)(f: NodeId => U): Unit = {

  }



  override protected [struct] def copy(plane: Plane): MatchIndex.this.type = ???
  override protected [struct] def onAddEntity(resultId: EntityId): Unit = ???
  override protected [struct] def onAddRef(resultId: NodeId, result: Node): Unit = ???
  override protected [struct] def onAddVal(resultId: NodeId, result: Node): Unit = ???
  override protected [struct] def onAddApp(resultId: NodeId, result: Node): Unit = ???
  override protected [struct] def onLink(appId: NodeId, app: Node.App, baseId: NodeId, base: Node): Unit = ???
  override protected [struct] def onLink(appId: NodeId, app: Node.App, argId: NodeId, arg: Node, roleId: NodeId, role: Node): Unit = ???
  override protected [struct] def onUnlink(nodeId: NodeId, node: Node, appId: NodeId, app: Node.App, index: Int): Unit = ???
  override protected [struct] def onRemove(nodeId: NodeId, node: Node): Unit = ???


  private sealed trait Traversable {
    def foreach[U](f: NodeId => U): Unit
  }

  private object All extends Traversable {
    override def foreach[U](f: NodeId => U): Unit = _plane.nodes.foreach(f)
  }

  private class Listed(items: Vector[NodeId]) extends Traversable {
    override def foreach[U](f: NodeId => U): Unit = items.foreach(f)
  }

  private class Partial(entries: LongMap[Int]) extends Traversable {
    override def foreach[U](f: NodeId => U): Unit = entries.foreachValue(_indices(_).foreach(f))
  }

  private class Full(entries: LongMap[Int]) extends Traversable {
    override def foreach[U](f: NodeId => U): Unit = entries.foreachValue(_indices(_).foreach(f))
  }

}
