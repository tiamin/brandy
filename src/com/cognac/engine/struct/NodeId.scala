package com.cognac.engine.struct

import com.cognac.fundamental.text.HexLiterals.LongToHexString

final case class NodeId(asLong: Long) extends AnyVal {

  @inline def isUndefined: Boolean = asLong == 0L

  @inline def isDefined: Boolean = asLong != 0L

  override def toString: String =
    if (isDefined) "#" + asLong.toHex8 else "Ø"

}

case object NodeId {
  val Undefined: NodeId = NodeId(0)
}
