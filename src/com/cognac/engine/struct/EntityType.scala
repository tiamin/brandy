package com.cognac.engine.struct

trait EntityType

object EntityType {

  case object Undefined extends EntityType

  case object Individual extends EntityType

  case object Parameter extends EntityType

}
