package com.cognac.engine.struct

import com.cognac.fundamental.text.EscapeExtensions.Escapes

import java.util.Objects
import scala.reflect.ClassTag.Null

trait Exp {
  def apply(args: (Exp, Exp)*): Exp.App = Exp.App(this, args)
  @inline final def asRef: Exp.Ref = this.asInstanceOf[Exp.Ref]
  @inline final def asVal: Exp.Val = this.asInstanceOf[Exp.Val]
  @inline final def asApp: Exp.App = this.asInstanceOf[Exp.App]
}

object Exp {

  final case class Val(value: AnyRef) extends Exp {
    override def toString: String = {
      val clazz = if (value != null) value.getClass else Null.runtimeClass
      val clazzName = clazz.getSimpleName
      val valueString = Objects.toString(value).escape
      s"$clazzName[$valueString]"
    }
  }

  final case class Ref(entityId: EntityId) extends Exp {
    override def toString: String = entityId.toString
  }


  final case class App(base: Exp, args: Iterable[(Exp, Exp)]) extends Exp {
    override def toString: String =
      s"$base(${ args.map { case (r, a) => s"$r: $a" }.mkString(", ") })"
  }

}