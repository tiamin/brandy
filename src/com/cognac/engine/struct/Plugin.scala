package com.cognac.engine.struct

trait Plugin {
  def plane: Plane
  protected [struct] def initialize(): Unit
  protected [struct] def copy(plane: Plane): this.type
  protected [struct] def onAddEntity(resultId: EntityId): Unit
  protected [struct] def onAddRef(resultId: NodeId, result: Node): Unit
  protected [struct] def onAddVal(resultId: NodeId, result: Node): Unit
  protected [struct] def onAddApp(resultId: NodeId, result: Node): Unit
  protected [struct] def onLink(appId: NodeId, app: Node.App, baseId: NodeId, base: Node): Unit
  protected [struct] def onLink(appId: NodeId, app: Node.App, argId: NodeId, arg: Node, roleId: NodeId, role: Node): Unit
  protected [struct] def onUnlink(nodeId: NodeId, node: Node, appId: NodeId, app: Node.App, index: Int): Unit
  protected [struct] def onRemove(nodeId: NodeId, node: Node): Unit
}
