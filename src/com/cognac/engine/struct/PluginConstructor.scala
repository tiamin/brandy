package com.cognac.engine.struct

import scala.reflect.runtime.universe._

trait PluginConstructor {
  type pluginType <: Plugin
  def pluginTpe: Type
  def id: String
  def apply(plane: Plane): pluginType
}
