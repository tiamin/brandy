package com.cognac.engine.struct

import com.cognac.fundamental.text.HexLiterals._
import com.cognac.fundamental.text.{ DetailPrinter, IndentedPrintWriter, SummaryPrinter, TextDeco }

final case class EntityId(asLong: Long) extends AnyVal {

  @inline def isUndefined: Boolean = asLong == 0L

  @inline def isDefined: Boolean = asLong != 0L

  @inline def isIndividual: Boolean = asLong > 0L

  @inline def isParameter: Boolean = asLong < 0L

  @inline def entityType: EntityType = asLong match {
    case 0 => EntityType.Undefined
    case _ if asLong < 0 => EntityType.Parameter
    case _ if asLong > 0 => EntityType.Individual
  }

  override def toString: String =
    if (isIndividual) "@" + asLong.toHex8 else if (isParameter) "?" + (-asLong).toHex else "Ø"
}

object EntityId {
  val Undefined: EntityId = EntityId(0)

  import com.cognac.fundamental.text.TextDeco._

  object Printer
    extends SummaryPrinter[EntityId]
      with DetailPrinter[EntityId] {
    override def print(entity: EntityId)
      (implicit indentedPrintWriter: IndentedPrintWriter, textDeco: TextDeco): Unit = {
      indentedPrintWriter.print(
        entity match {
          case x if x.isUndefined => "Ø".BrRed
          case x if x.isParameter => x.toString.BrMagenta
          case x => x.toString.Yellow
        }
      )
    }
  }

}
