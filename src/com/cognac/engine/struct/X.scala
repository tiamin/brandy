package com.cognac.engine.struct
import com.cognac.engine.struct
import com.cognac.fundamental.collection.IterableSymbol.*

import scala.reflect.runtime.universe._

object X {

  def main(args: Array[String]): Unit = {
    val individualIds = Iterator.from(1, 1).map(EntityId(_))
    val parameterIds = Iterator.from(-1, -1).map(EntityId(_))
    val nodeIds = Iterator.from(1, 1).map(NodeId(_))
    val p = new MapPlane({
      case EntityType.Individual => individualIds
      case EntityType.Parameter => parameterIds
      case _ => throw new RuntimeException
    }, nodeIds)

    import Exp._
    p.load(Ref(p.addEntity(EntityType.Individual))(
      Ref(EntityId(154L)) -> Ref(EntityId(200L)),
      Ref(EntityId(43L)) -> Ref(EntityId(200L))
    ))

    println(mi)

  }
}
