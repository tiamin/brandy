package com.cognac.engine.struct
import com.cognac.fundamental.collection.IterableSymbol.*

import scala.collection.immutable.LongMap

class MapPlane(entityIdGenSelector: EntityType => Iterator[EntityId], nodeIdGen: Iterator[NodeId]) extends Plane {

  private var _expressions: LongMap[Node] = LongMap.empty

  override def nodes: *[NodeId] =
    _expressions.view.map { case (k, _) => NodeId(k) }

  override def apply(nodeId: NodeId): Node =
    _expressions.getOrElse(nodeId.asLong, null)

  override def addEntity(entityType: EntityType): EntityId = {
    val entityId = entityIdGenSelector(entityType).next()
    plugins.foreach { case (_, _, plugin) => plugin.onAddEntity(entityId) }
    entityId
  }

  override def addRef(entityId: EntityId): NodeId =
    add(Node.Ref(NodeId.Undefined, entityId))

  override def addVal(value: AnyRef): NodeId =
    add(Node.Val(NodeId.Undefined, value))

  override def addApp(): NodeId =
    add(Node.App(NodeId.Undefined, NodeId.Undefined, Array.empty))

  override def link(appId: NodeId, baseId: NodeId): Unit = rollbackOnException {
    val base = apply(baseId)
    require(base.hasNoParent)
    val app = asApp(appId)
    require(app.baseId.isUndefined)
    set(appId, app withBase baseId)
    set(baseId, base withParent appId)
    plugins.foreach { case (_, _, plugin) => plugin.onLink(appId, app, baseId, base) }
  }

  override def link(appId: NodeId, argId: NodeId, roleId: NodeId): Unit = rollbackOnException {
    val arg = apply(argId)
    require(arg.hasNoParent)
    val role = apply(roleId)
    require(role.hasNoParent)
    val app = asApp(appId)
    require(app.argIndex(argId) < 0)
    require(app.roleIndex(roleId) < 0)
    set(appId, app withArg (argId, roleId))
    set(argId, arg withParent appId)
    set(roleId, role withParent appId)
    plugins.foreach { case (_, _, plugin) => plugin.onLink(appId, app, argId, arg, roleId, role) }
  }

  override def unlink(nodeId: NodeId): Unit = rollbackOnException {
    val node = apply(nodeId)
    val parentId = node.parentId
    require(parentId.isDefined)
    val parent = asApp(parentId)
    if (parent.baseId == nodeId) {
      val (baseId, base) = (nodeId, node)
      set(parentId, parent withBase NodeId.Undefined)
      set(baseId, base withParent NodeId.Undefined)
      plugins.foreach { case (_, _, plugin) => plugin.onUnlink(nodeId, node, parentId, parent, -1) }
    } else {
      val argIdx = parent.argIndex(nodeId)
      if (0 <= argIdx) {
        val (argId, arg) = (nodeId, node)
        val roleId = parent.role(argIdx)
        val role = apply(roleId)
        set(parentId, parent withoutArg argIdx)
        set(argId, arg withParent NodeId.Undefined)
        set(roleId, role withParent NodeId.Undefined)
        plugins.foreach { case (_, _, plugin) => plugin.onUnlink(nodeId, node, parentId, parent, argIdx) }
      } else {
        val roleIdx = parent.roleIndex(nodeId)
        val argId = parent.arg(roleIdx)
        val arg = apply(argId)
        val (roleId, role) = (nodeId, node)
        set(parentId, parent withoutArg argIdx)
        set(argId, arg withParent NodeId.Undefined)
        set(roleId, role withParent NodeId.Undefined)
        plugins.foreach { case (_, _, plugin) => plugin.onUnlink(nodeId, node, parentId, parent, roleIdx) }
      }
    }
  }

  override def remove(nodeId: NodeId): Unit = rollbackOnException {
    val node = apply(nodeId)
    require(node.hasNoParent)
    node match {
      case app: Node.App =>
        require(app.baseId.isUndefined)
        require(app.rawArgs.isEmpty)
      case _ =>
    }
    _expressions -= nodeId.asLong
    plugins.foreach { case (_, _, plugin) => plugin.onRemove(nodeId, node) }
  }

  override def copy: Plane = {
    val cpy = new MapPlane(entityIdGenSelector, nodeIdGen)
    cpy._plugins = this._plugins.map(e => e.copy(_3 = e._3.copy(cpy)))
    cpy._expressions = this._expressions
    cpy
  }

  @inline private def rollbackOnException[A](f: => A): A = {
    val expressionsSave = _expressions
    try {
      f
    } catch {
      case t: Throwable =>
        _expressions = expressionsSave
        throw t
    }
  }

  @inline private def asApp(nodeId: NodeId): Node.App = apply(nodeId) match {
    case a: Node.App => a
    case _ => throw new IllegalArgumentException("not an app")
  }

  @inline private def add(node: Node): NodeId = {
    val id = nodeIdGen.next()
    set(id, node)
    plugins.foreach { case (_, _, plugin) => plugin.onAddVal(id, node) }
    id
  }

  @inline private def set(id: NodeId, node: Node): Unit =
    _expressions += id.asLong -> node


}
